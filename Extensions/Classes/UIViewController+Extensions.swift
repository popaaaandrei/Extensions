//
//  UIViewController+Extensions.swift
//
//
//  Created by andrei on 25/11/2017.
//  Copyright © 2016 andrei. All rights reserved.
//

import UIKit





// ============================================================================
// MARK: UIViewController
// ============================================================================
public extension UIViewController {
    
    func checkIfSure(title: String = "Confirm",
                     message: String,
                     handler: @escaping (() -> ())) {
        
        // if iPad -> alert, else: action sheet
        let style : UIAlertController.Style = UIDevice.current.userInterfaceIdiom == .pad ? .alert : .actionSheet
        
        // Declare Alert
        let dialogMessage = UIAlertController(title: title, message: message, preferredStyle: style)
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .destructive, handler: { (action) -> Void in
            handler()
        })
        
        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "Cancel", style: .cancel) { (action) -> Void in
            
        }
        
        // Add OK and Cancel button to dialog message
        dialogMessage.addAction(ok)
        dialogMessage.addAction(cancel)
        
        // dialogMessage.popoverPresentationController?.sourceView
        
        // Present dialog message to user
        present(dialogMessage, animated: true, completion: nil)
    }
    
    
    
    class func top() -> UIViewController? {
        var topController = UIApplication.shared.keyWindow?.rootViewController
        while topController?.presentedViewController != nil {
            topController = topController?.presentedViewController
        }
        return topController
    }
    
    
    /// identifier = the class name
    static var identifier: String {
        return NSStringFromClass(self.classForCoder()).components(separatedBy: ".").last!
    }
    
    /// factory
    static func generate<T : UIViewController>(type: T.Type,
                                               storyboard: String = "Main",
                                               bundle: Bundle? = nil) -> T {
        
        return UIStoryboard(name: storyboard, bundle: bundle)
            .instantiateViewController(withIdentifier: T.identifier) as! T
    }
    
    func setBarButton(title: String?,
                      font: UIFont,
                      imageNormal: String,
                      imageSelected: String,
                      titleNormalColor: UIColor,
                      titleSelectedColor: UIColor,
                      selectedRenderingMode: UIImage.RenderingMode = .alwaysTemplate) {
        
        let normalImage = UIImage(named: imageNormal)?
            .resizeAspectFit(size: CGSize(width: 50, height: 50))?
            .withRenderingMode(UIImage.RenderingMode.alwaysOriginal)
        
        let normalSelectedImage = UIImage(named: imageSelected)?
            .resizeAspectFit(size: CGSize(width: 50, height: 50))?
            .withRenderingMode(selectedRenderingMode)
        
        guard case _? = normalImage else {
            print("setBarButton: '\(imageNormal)' not found")
            return
        }
        
        
        let margin : CGFloat = 6.0
        
        if let title = title {
            self.tabBarItem = UITabBarItem(title: title, image: normalImage, selectedImage: normalSelectedImage)
            self.tabBarItem.image = normalImage
            
            self.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: font,
                                                    NSAttributedString.Key.foregroundColor: titleNormalColor], for: UIControl.State.normal)
            self.tabBarItem.setTitleTextAttributes([NSAttributedString.Key.font: font,
                                                    NSAttributedString.Key.foregroundColor: titleSelectedColor], for: UIControl.State.selected)
        }
        else {
            self.tabBarItem = UITabBarItem(title: nil, image: normalImage, selectedImage: normalSelectedImage)
            self.tabBarItem.image = normalImage
            self.tabBarItem.imageInsets = UIEdgeInsets(top: margin, left: 0.0, bottom: -margin, right: 0.0)
        }
        
    }
    
    
    /// finds nearest UITabBarController
    func nearestTabBarController() -> UITabBarController? {
        if self is UITabBarController {
            return self as? UITabBarController
        }
        
        let parentVC = parent != nil ? parent : presentingViewController
        return parentVC?.nearestTabBarController()
    }
    
    func addBackButton() {
        let backBBItem = UIBarButtonItem(image: UIImage(named: "back_arrow"), style: .done, target: self, action: #selector(navigationBackAction))
        self.navigationItem.leftBarButtonItem = backBBItem
    }
    
    @objc func navigationBackAction() {
        _ = self.navigationController?.popViewController(animated: true)
    }
}
