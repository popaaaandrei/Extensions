//
//  APResult.swift
//
//  Created by andrei on 20/07/2019.
//  Copyright © 2016 andrei. All rights reserved.
//

import Foundation



/// possible result of operation
public enum APResult {
    case started(message: String?)
    case success(message: String?)
    case message(String)
    case error(Error)
    case progress
    case stop
}

