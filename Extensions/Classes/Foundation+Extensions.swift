//
//  Extensions.swift
//
//
//  Created by andrei on 25/11/2017.
//  Copyright © 2016 andrei. All rights reserved.
//

import Foundation



public extension Array {
    subscript (safe index: Index) -> Element? {
        return 0 <= index && index < count ? self[index] : nil
    }
}


// ============================================================================
// Bytes
// ============================================================================
public extension Array where Element == UInt8 {
    
    func asStringBase64() -> String {
        return Data(self).base64EncodedString()
    }
    
    func asStringUTF8() -> String? {
        return String(data: Data(self), encoding: .utf8)
    }
    
    func asData() -> Data {
        return Data(self)
    }
    
}


// ============================================================================
// Data
// ============================================================================
public extension Data {
    
    /// Data -> [UInt8]
    func asBytes() -> [UInt8] {
        return Array(self)
    }
    
    /// Data -> String Base64
    func asStringBase64() -> String {
        return self.base64EncodedString()
    }
    
    /// Data -> String UTF8
    func asStringUTF8() -> String? {
        return String(data: self, encoding: .utf8)
    }
    
}


// ============================================================================
// String
// ============================================================================
public extension String {
    
    func asDataUTF8() -> Data? {
        return data(using: .utf8)
    }
    
    func asDataBase64() -> Data? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        return data
    }
    
    func asBytesBase64() -> [UInt8]? {
        guard let data = Data(base64Encoded: self, options: .ignoreUnknownCharacters) else {
            return nil
        }
        return data.asBytes()
    }
    
    func endsWithSpaces() -> Bool {
        if let range = rangeOfCharacter(from: .whitespacesAndNewlines),
            range.upperBound == endIndex {
            return true
        } else {
            return false
        }
    }
    
    func beginsWithSpaces() -> Bool {
        if let range = rangeOfCharacter(from: .whitespacesAndNewlines),
            range.lowerBound == startIndex {
            return true
        } else {
            return false
        }
    }
    
    func escapedForMqtt() -> String {
        return self.replacingOccurrences(of: "+", with: "-")
            .replacingOccurrences(of: "/", with: "_")
            .replacingOccurrences(of: "=", with: "")
    }
    
}



// ============================================================================
// MARK: Bool
// ============================================================================
public extension Bool {
    mutating func toggle() {
        self = !self
    }
}



// ============================================================================
// MARK: String
// ============================================================================
public extension String {
    
    func size(constrainedToWidth width: CGFloat? = nil,
              height: CGFloat? = nil,
              font: UIFont) -> CGSize {
        
        var constraintRect = UIScreen.main.bounds.size
        if let width = width {
            constraintRect.width = width
        }
        if let height = height {
            constraintRect.height = height
        }
        
        return NSString(string: self)
            .boundingRect(with: constraintRect,
                          options: NSStringDrawingOptions.usesLineFragmentOrigin,
                          attributes: [NSAttributedString.Key.font: font],
                          context: nil).size
    }
    
    
    /// return calculated maximum font size that fits a box
    func calculateMaximumFontSize(box: CGSize, boxPercent: CGFloat, fontName: String) -> CGFloat {
        
        // start values
        var fontSize : CGFloat = 7
        var boundingSize = CGSize.zero
        let restrictedBox = CGSize(width: box.width * boxPercent, height: box.height * boxPercent)
        
        // if we have a valid string
        guard self.count > 0 else {
            return fontSize
        }
        
        // check whether the font exists, else return minimum size
        guard let _ = UIFont(name: fontName, size: fontSize) else {
            return fontSize
        }
        
        while restrictedBox.height > boundingSize.height  &&
            restrictedBox.width > boundingSize.width  {
                
                fontSize += 1
                let textFontSize = fontSize + 1
                
                let font = UIFont(name: fontName, size: textFontSize)!
                
                let attributes = [NSAttributedString.Key.font: font, NSAttributedString.Key.foregroundColor: UIColor.black]
                
                boundingSize = NSAttributedString(string: self, attributes: attributes).boundingRect(with: restrictedBox, options: NSStringDrawingOptions.usesFontLeading, context: nil).size
        }
        
        return fontSize
    }
    
    func trim() -> String {
        return self.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    }
    
    func trim(charactersInString: String) -> String {
        return self.trimmingCharacters(in: CharacterSet(charactersIn: charactersInString))
    }

    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func width(considering height: CGFloat, and font: UIFont) -> CGFloat {
        let constraintBox = CGSize(width: .greatestFiniteMagnitude, height: height)
        let rect = self.boundingRect(with: constraintBox, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        return rect.width
    }
    
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
}



// ============================================================================
// MARK: FileManager
// ============================================================================
public extension FileManager {
    
    func enumerate(recursivelyAtPath path: String) {
        let enumerator = FileManager.default.enumerator(atPath: path)
        while let element = enumerator?.nextObject() as? String {
            print("file: \(element)")
        }
    }

}


